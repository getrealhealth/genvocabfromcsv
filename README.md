﻿Generate PHR Vocabulary Xml from CSV
====================================

Usage:

`GenVocabFromCSV <vocabulary-name> <CSV-file-path> [<output-directory>]`

`<vocabulary-name>` format

Default grc.phr family vocabulary       eg. vocab-name

Include Vocabulary family       eg. "family|vocab-name"

- Make sure the CSV file has headers which corresponds to the locale (ex. en-US, nl-NL)
- If `code-value` is available, make sure to set the column header as 'code'

You can export CSV directly after editing the spreadsheet to specification given above.
In google docs, follow:
*File -> Download -> Comma-seperated values (.csv, current sheet)*

**Find the ready to use executable within the root folder**

-----------

### DEVELOPER NOTES

- Publish profile has been included with optimum settings to generage self-contained executable
- The XSD used to generate the `VocabularyType` class is also included
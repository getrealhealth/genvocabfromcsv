﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace GenVocabFromCSV
{
    class Program
    {
        static void Main(string[] args)
        {
            string csvText = string.Empty;
            string vocabularyFamily = "grc.phr";
            string vocabularyName = string.Empty;
            if (args.Length > 1)
            {
                try
                {
                    csvText = File.ReadAllText(args[1]);
                }
                catch (Exception e)
                {
                    throw new Exception("Please provide valid the path to CSV", e);
                }
                vocabularyName = args[0].Trim('"');
                if (vocabularyName.Contains('|'))
                {
                    vocabularyFamily = vocabularyName.Split('|')[0];
                    vocabularyName = vocabularyName.Split('|')[1];
                }
            }
            else
            {
                Console.WriteLine("Usage: GenVocabFromCSV <vocabulary-name> <CSV-file-path> [<output-directory>]");
                Console.WriteLine("\n<vocabulary-name> format ");
                Console.WriteLine("Default grc.phr family vocabulary \teg. vocab-name");
                Console.WriteLine("Include Vocabulary family \teg. \"family|vocab-name\"");
                return;
            }

            string pathToOutputDirectory = string.Empty;
            if (args.Length > 2)
            {
                pathToOutputDirectory = args[2].Trim('"');
                if (!Directory.Exists(pathToOutputDirectory))
                {
                    throw new Exception("Output directory does not exist!");
                }
            }

            List<string> allLines = csvText.Split("\r\n").ToList();
            List<string> headers = allLines.First().Split(',').Select(h => h.Trim('"')).ToList();
            int codeIndex = headers.IndexOf("code");
            allLines.RemoveAt(0);

            for (int i = 0; i < headers.Count; i++)
            {
                // refrain from the routine if invalid or 'code' column is found
                if (string.IsNullOrEmpty(headers[i]) || i == codeIndex)
                {
                    continue;
                }

                //split csv and remove enclosures
                List<string> vocabItemDisplays = GetCSVColumn(allLines, i);

                // if output directory not mentioned, outputs to current directory
                string fileName = string.Format(
                    "{0}" + (string.IsNullOrEmpty(pathToOutputDirectory) ? string.Empty : "\\") + "{1}.{2}.xml",
                    pathToOutputDirectory,
                    vocabularyName,
                    headers[i].TrimStart('.'));

                VocabularyType vocab = GetPopulatedVocabTypeFromItemDisplays(
                    vocabularyFamily,
                    vocabularyName,
                    vocabItemDisplays,
                    GetCSVColumn(allLines, codeIndex));
                var serializer = new XmlSerializer(typeof(VocabularyType));
                using (var fileStream = File.Create(fileName))
                {
                    Console.WriteLine(string.Format("Writing vocabulary file to {0}", fileStream.Name));
                    serializer.Serialize(fileStream, vocab);
                }
            }
        }

        private static List<string> GetCSVColumn(List<string> csvLines, int index)
        {
            if (index == -1)
            {
                return null;
            }
            return csvLines.Select(l => l.Split(',')[index].Trim('"').Trim(' ')).ToList();
        }

        private static VocabularyType GetPopulatedVocabTypeFromItemDisplays(
            string vocabularyFamily,
            string vocabularyName,
            List<string> vocabItemDisplays,
            List<string> codes)
        {
            var vocabulary = new VocabularyType()
            {
                vocabularyformatversion = 1,
                name = vocabularyName,
                family = vocabularyFamily,
                version = 1,
                items = new itemType[vocabItemDisplays.Count]
            };

            for (int i = 0; i < vocabItemDisplays.Count; i++)
            {
                string codeValue = string.Empty;
                if (codes != null)
                {
                    codeValue = codes[i];
                }
                else
                {
                    string codePad = new string('0', (int)Math.Floor(Math.Log10(vocabItemDisplays.Count)) + 2);
                    codeValue = string.Format("{0:" + codePad + "}", i + 1);
                }

                vocabulary.items[i] = new itemType() {codevalue = codeValue, displaytext = vocabItemDisplays.ElementAt(i) };
            }

            return vocabulary;
        }
    }
}
